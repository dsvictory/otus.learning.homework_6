﻿using System;
using System.Collections.Generic;
using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions;
using Otus.Teaching.PromoCodeFactory.Core.Application;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Xunit;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using AutoFixture.Kernel;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;
        private readonly PartnersController _partnersController;

        private readonly IRepository<Partner> _inMemoryPartnersRepository;
        private readonly PartnersController _partnersControllerWithInMemoryRepository;
        private readonly Mock<ILimitsSetter> _limitsSetter;

        public SetPartnerPromoCodeLimitAsyncTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnersRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
            fixture.Freeze<Mock<ICurrentDateTimeGetter>>();
            _partnersController = fixture.Build<PartnersController>().OmitAutoProperties().Create();


            var fixtureForInMemoryVariant = new Fixture().Customize(new AutoMoqCustomization());
            _inMemoryPartnersRepository = InMemoryRepositoryHelper.GetInMemoryRepository<Partner>();
            fixtureForInMemoryVariant.Inject(_inMemoryPartnersRepository);

            _limitsSetter = fixtureForInMemoryVariant.Freeze<Mock<ILimitsSetter>>();

            fixtureForInMemoryVariant.Freeze<Mock<ICurrentDateTimeGetter>>();       
            _partnersControllerWithInMemoryRepository = fixtureForInMemoryVariant.Build<PartnersController>().OmitAutoProperties().Create();
        }

        private Partner CreateBasePartner()
        {
            var partner = new Partner()
            {
                Id = Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8"),
                Name = "Суперигрушки",
                NumberIssuedPromoCodes = 15,
                IsActive = true,
                PartnerLimits = new List<PartnerPromoCodeLimit>()
                {
                    new PartnerPromoCodeLimit()
                    {
                        Id = Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116393"),
                        CreateDate = new DateTime(2020, 07, 9),
                        EndDate = new DateTime(2020, 10, 9),
                        Limit = 100
                    }
                }
            };

            return partner;
        }

        private SetPartnerPromoCodeLimitRequest CreateBaseSetLimitRequest() 
            => new SetPartnerPromoCodeLimitRequest {
                Limit = 30,
                EndDate = new DateTime(2022, 1, 1)
            };

        [Fact]
        public async void  SetPartnerPromoCodeLimitAsyncTests_PartnerNotFound_ReturnsNotFound() {

            // Arrange
            var partnerId = Guid.NewGuid();
            Partner partnerAbsense = null;
            var setLimitRequest = CreateBaseSetLimitRequest();
            
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(It.IsAny<Guid>()))
                .ReturnsAsync(partnerAbsense);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, setLimitRequest);
 
            // Assert
            result.Should().BeAssignableTo<NotFoundResult>();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsyncTests_PartnerIsNotActive_ReturnsStatus400() {

            // Arrange
            Partner partner = CreateBasePartner(); 
            partner.IsActive = false;

            var setLimitRequest = CreateBaseSetLimitRequest();
            
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(It.IsAny<Guid>()))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, setLimitRequest);
 
            // Assert
            var requestResult = result.Should().BeAssignableTo<BadRequestObjectResult>().Which;
            requestResult.StatusCode.Should().Be(400);
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsyncTests_SettingLimitLowerThanZero_ReturnsStatus400() {

            // Arrange
            Partner partner = CreateBasePartner();
            var invalidSetLimitRequest = new SetPartnerPromoCodeLimitRequest { Limit = -5 };
            
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(It.IsAny<Guid>()))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, invalidSetLimitRequest);
 
            // Assert
            var requestResult = result.Should().BeAssignableTo<BadRequestObjectResult>().Which;
            requestResult.StatusCode.Should().Be(400);      
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsyncTests_AllOperationsWerePassed_PartnerWithNewLimitWasSaved() {

            // Arrange
            Partner partner = CreateBasePartner();
            var setLimitRequest = CreateBaseSetLimitRequest();
            var newPromocodeLimit = new PartnerPromoCodeLimit {
                Limit = 78787878
            };

            int newLimit = newPromocodeLimit.Limit;

            await _inMemoryPartnersRepository.AddAsync(partner);

            _limitsSetter.Setup(x => x.AddNewLimit(It.IsAny<Partner>(), It.IsAny<PartnerPromoCodeLimit>()))
                .Callback(() => 
                { 
                    partner.PartnerLimits.Add(newPromocodeLimit); 
                });

            // Act
            var result = await _partnersControllerWithInMemoryRepository.SetPartnerPromoCodeLimitAsync(partner.Id, setLimitRequest);
            var partnerFromRepository = await _inMemoryPartnersRepository.GetByIdAsync(partner.Id);
 
            // Assert
            partnerFromRepository.PartnerLimits.FirstOrDefault(x => 
                x.Limit == newLimit
            )
            .Should().NotBeNull();
        }
    }
}