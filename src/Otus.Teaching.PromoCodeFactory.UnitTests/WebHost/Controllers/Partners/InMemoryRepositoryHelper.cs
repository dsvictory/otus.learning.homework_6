﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.DataAccess;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;


namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners {
    public static class InMemoryRepositoryHelper {

        public static IRepository<T> GetInMemoryRepository<T>() where T : BaseEntity 
        {
            var inMemoryDbOptions = new DbContextOptionsBuilder<DataContext>()
                .UseInMemoryDatabase("TestDb").Options;

            return new EfRepository<T>(new DataContext(inMemoryDbOptions));
        }
    }
}
