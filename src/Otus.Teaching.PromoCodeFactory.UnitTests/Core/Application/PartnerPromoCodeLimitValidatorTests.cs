﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoFixture;
using AutoFixture.AutoMoq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Application;
using Xunit;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System.Linq;
using FluentValidation.TestHelper;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Core.Application {
    public class PartnerPromoCodeLimitValidatorTests {

        private readonly Mock<ICurrentDateTimeGetter> _currentDateTimeGetterMock;
        private readonly PartnerPromoCodeLimitValidator _limitValidator;

        public PartnerPromoCodeLimitValidatorTests() {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _currentDateTimeGetterMock = fixture.Freeze<Mock<ICurrentDateTimeGetter>>();
            _limitValidator = fixture.Build<PartnerPromoCodeLimitValidator>().OmitAutoProperties().Create();
        }

        private PartnerPromoCodeLimit CreateBaseLimit()
        {
            return new PartnerPromoCodeLimit()
                    {
                        Id = Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116393"),
                        CreateDate = new DateTime(2020, 07, 9),
                        EndDate = new DateTime(2020, 10, 9),
                        Limit = 100
                    };     
        }


        [Theory]
        [InlineData(-10, true)]
        [InlineData(0, true)]
        [InlineData(1, false)]
        [InlineData(100, false)]
        public async void Validate_LimitLowerThanZero_HasErrorForLimit(int limit, bool errorMustBe) {

            // Arrange
            var promoCodeLimit = CreateBaseLimit();
            promoCodeLimit.Limit = limit;

            // Act
            var result = await _limitValidator.TestValidateAsync(promoCodeLimit);
 
            // Assert
            if (errorMustBe) {
                result.ShouldHaveValidationErrorFor(x => x.Limit);
            } else {
                result.ShouldNotHaveValidationErrorFor(x => x.Limit);
            }
        }

        [Theory]
        [InlineData(1, false)]
        [InlineData(0, true)]
        [InlineData(-1, true)]
        public async void Validate_EndDateLowerThanCurrent_HasErrorForEndDate(int offsetInDays, bool errorMustBe) {

            // Arrange
            var limit = CreateBaseLimit();

            var currentDate = new DateTime(2021, 1, 20);
            _currentDateTimeGetterMock.Setup(x => x.CurrentDateTime).Returns(currentDate);

            limit.EndDate = currentDate.AddDays(offsetInDays);

            // Act
            var result = await _limitValidator.TestValidateAsync(limit);

            // Assert
            if (errorMustBe) {
                result.ShouldHaveValidationErrorFor(x => x.EndDate);
            }
            else {
                result.ShouldNotHaveValidationErrorFor(x => x.EndDate);
            }
        }

    }
}
