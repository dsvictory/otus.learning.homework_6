﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoFixture;
using AutoFixture.AutoMoq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Application;
using Xunit;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System.Linq;
using FluentValidation;
using FluentValidation.Results;
using System.Threading;
using System.Threading.Tasks;
using FluentAssertions;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Core.Application {
    public class LimitsSetterTests {

        private readonly LimitsSetter _limitsSetter;

        private readonly Mock<ICurrentDateTimeGetter> _currentDateTimeGetterMock;

        private readonly Mock<IValidator<PartnerPromoCodeLimit>> _limitValidator;

        public LimitsSetterTests() {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _currentDateTimeGetterMock = fixture.Freeze<Mock<ICurrentDateTimeGetter>>();
            _limitValidator = fixture.Freeze<Mock<IValidator<PartnerPromoCodeLimit>>>();
            _limitsSetter = fixture.Build<LimitsSetter>().OmitAutoProperties().Create();

            _currentDateTimeGetterMock.Setup(x => x.CurrentDateTime).Returns(new DateTime(2021, 1, 1));

            var validationResult = new Mock<ValidationResult>();
            validationResult.Setup(x => x.IsValid).Returns(true);

            _limitValidator.Setup(x => x.ValidateAsync(It.IsAny<PartnerPromoCodeLimit>(), It.IsAny<CancellationToken>()))
                .Returns(Task.FromResult(validationResult.Object));
        }

        private Partner CreateBasePartner()
        {
            var partner = new Partner()
            {
                Id = Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8"),
                Name = "Суперигрушки",
                NumberIssuedPromoCodes = 15,
                IsActive = true,
                PartnerLimits = new List<PartnerPromoCodeLimit>()
                {
                    new PartnerPromoCodeLimit()
                    {
                        Id = Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116393"),
                        EndDate = new DateTime(2022, 10, 9),
                        CreateDate = new DateTime(2020, 1, 1),
                        Limit = 20
                    }
                }
            };

            return partner;
        }

        private PartnerPromoCodeLimit CreateBaseLimit() {
            return new PartnerPromoCodeLimit()
                    {
                        Id = Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab114444"),
                        CreateDate = new DateTime(2020, 07, 9),
                        EndDate = new DateTime(2022, 10, 9),
                        Limit = 100
                    };
        }

        private void SetAllLimitsCancelled(Partner partner) {
            foreach (var limit in partner.PartnerLimits) {
                limit.CancelDate = DateTime.MaxValue;
            }
        }

        [Fact]
        public async void AddNewLimit_LimitIsNotValid_ThrowsException() {
            // Arrange
            var partner = CreateBasePartner();
            var newLimit = CreateBaseLimit();

            var validationResult = new Mock<ValidationResult>();
            validationResult.Setup(x => x.IsValid).Returns(false);

            _limitValidator.Setup(x => x.ValidateAsync(It.IsAny<PartnerPromoCodeLimit>(), It.IsAny<CancellationToken>()))
                .Returns(Task.FromResult(validationResult.Object));

            // Act
            Func<Task> action = () => _limitsSetter.AddNewLimit(partner, newLimit);
 
            // Assert
            await action.Should().ThrowAsync<ApplicationLevelException>();
        }

        [Fact]
        public async void AddNewLimit_PartnerAlreadyHasActiveLimit_IssuedPromocodesMustBeSettedToZero() {

            // Arrange
            Partner partnerWithActiveLimit = CreateBasePartner();
            var setLimitRequest = CreateBaseLimit();

            // Act
            await _limitsSetter.AddNewLimit(partnerWithActiveLimit, setLimitRequest);
 
            // Assert
            partnerWithActiveLimit.NumberIssuedPromoCodes
                .Should().Be(0);
        }

        [Fact]
        public async void AddNewLimit_PartnerHasNotActiveLimit_IssuedPromocodesMustNotBeChanged() {

            // Arrange
            Partner partner = CreateBasePartner();

            SetAllLimitsCancelled(partner);

            var setLimitRequest = CreateBaseLimit();

            var startNumberIssuedPromoCodes = partner.NumberIssuedPromoCodes;
            
            // Act
            await _limitsSetter.AddNewLimit(partner, setLimitRequest);
 
            // Assert
            partner.NumberIssuedPromoCodes
                .Should().Be(startNumberIssuedPromoCodes);
        }

        [Fact]
        public async void AddNewLimit_PartnerAlreadyHasActiveLimit_ExistingLimitMustBeCancelled() {

            // Arrange
            Partner partner = CreateBasePartner();
            var setLimitRequest = CreateBaseLimit();
            
            var activeLimit = partner.PartnerLimits.FirstOrDefault(x => 
                !x.CancelDate.HasValue);

            // Act
            await _limitsSetter.AddNewLimit(partner, setLimitRequest);
 
            // Assert
            activeLimit.CancelDate.Should().NotBeNull();
        }

    }
}
