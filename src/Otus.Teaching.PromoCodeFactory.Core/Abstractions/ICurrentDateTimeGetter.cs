﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions {
    public interface ICurrentDateTimeGetter {

        DateTime CurrentDateTime { get; }
    }
}
