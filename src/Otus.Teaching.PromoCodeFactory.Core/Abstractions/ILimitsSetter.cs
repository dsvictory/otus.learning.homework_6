﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions {
    public interface ILimitsSetter {

        Task AddNewLimit(Partner partner, PartnerPromoCodeLimit limit);

    }
}
