﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using FluentValidation;
using FluentValidation.Results;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions;

namespace Otus.Teaching.PromoCodeFactory.Core.Application {
    public class PartnerPromoCodeLimitValidator : AbstractValidator<PartnerPromoCodeLimit> {
        
       private readonly ICurrentDateTimeGetter _currentDateTimeGetter;

       public PartnerPromoCodeLimitValidator(ICurrentDateTimeGetter currentDateTimeGetter) {

            _currentDateTimeGetter = currentDateTimeGetter;

            RuleFor(x => x.Limit).GreaterThan(0)
                .WithMessage("{PropertyName} must be greater than {ComparisonValue}, but was got {PropertyValue}");

            RuleFor(x => x.EndDate).GreaterThan(x => _currentDateTimeGetter.CurrentDateTime)
                .WithMessage("{PropertyName} must be greater than current datetime ({ComparisonValue}), but was got {PropertyValue}");
       }

    }
}
