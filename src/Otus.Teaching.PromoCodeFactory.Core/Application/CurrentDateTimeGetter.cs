﻿using System;
using System.Collections.Generic;
using System.Text;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions;

namespace Otus.Teaching.PromoCodeFactory.Core.Application {
    public class CurrentDateTimeGetter : ICurrentDateTimeGetter {

        public DateTime CurrentDateTime => DateTime.Now;
    }
}
