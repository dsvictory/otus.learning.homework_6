﻿using System;
using System.Collections.Generic;
using System.Text;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System.Linq;
using FluentValidation;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.Core.Application {
    public class LimitsSetter : ILimitsSetter {

        private readonly IValidator<PartnerPromoCodeLimit> _validator;

        private readonly ICurrentDateTimeGetter _currentDateTimeGetter;

        public LimitsSetter(IValidator<PartnerPromoCodeLimit> validator,
            ICurrentDateTimeGetter currentDateTimeGetter) 
        {
            _validator = validator;
            _currentDateTimeGetter = currentDateTimeGetter;
        }

        public async Task AddNewLimit(Partner partner, PartnerPromoCodeLimit limit) {
            
            var validateResult = await _validator.ValidateAsync(limit);
            if (!validateResult.IsValid) {
                throw new ApplicationLevelException(validateResult.ToString());
            }

            //Установка лимита партнеру
            var activeLimit = partner.PartnerLimits.FirstOrDefault(x => 
                !x.CancelDate.HasValue);
            
            if (activeLimit != null)
            {
                //Если партнеру выставляется лимит, то мы 
                //должны обнулить количество промокодов, которые партнер выдал, если лимит закончился, 
                //то количество не обнуляется
                partner.NumberIssuedPromoCodes = 0;
                
                //При установке лимита нужно отключить предыдущий лимит
                activeLimit.CancelDate = _currentDateTimeGetter.CurrentDateTime;
            }
                   
            partner.PartnerLimits.Add(limit);
        }
    }
}
