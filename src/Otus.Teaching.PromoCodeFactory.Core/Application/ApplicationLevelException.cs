﻿using System;
using System.Collections.Generic;
using System.Text;

#nullable enable

namespace Otus.Teaching.PromoCodeFactory.Core.Application {
    public class ApplicationLevelException : Exception {

        public ApplicationLevelException(string? message) : base(message) { }

    }
}
